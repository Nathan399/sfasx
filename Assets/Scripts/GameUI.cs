﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUI : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] GameObject AbilityBar;

    [SerializeField] GameObject PlayerTurnText;
    [SerializeField] GameObject EnemyTurnText;

    void awake()
    {
    }

    public void ChangeToPlayerTurn()
    {
        PlayerTurnText.GetComponent<Animator>().SetTrigger("TextDown"); //hide enemy turn 
        EnemyTurnText.GetComponent<Animator>().SetTrigger("TextUp");    //show player turn
    }

    public void ChangeToEnemyTurn()
    {
        PlayerTurnText.GetComponent<Animator>().SetTrigger("TextUp");   //hide player turn 
        EnemyTurnText.GetComponent<Animator>().SetTrigger("TextDown");  //show enemy turn
    }

    public GameObject GetAbilityBar()
    {
        return AbilityBar;
    }
}

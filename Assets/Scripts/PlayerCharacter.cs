﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : Character
{
    public BaseSpell[] Spells;
    List<GameObject> Buttons;

    [SerializeField] private AudioClip SpellSound;

    // Start is called before the first frame update
    public override void Awake()
    {
       Spells = transform.GetComponents<BaseSpell>();
       Buttons = new List<GameObject>();
       base.Awake();
    }

    public override void ShowSkillHotbar()
    {
        Game game = GameObject.Find("Game").GetComponent<Game>();
        GameObject abilitybar = game.GameUI.GetAbilityBar();

        int i = 0;
        foreach (BaseSpell spell in Spells) 
        {
            var button = Resources.Load("AbilityButton") as GameObject; //set the information for the skill bar
            var but = Instantiate(button, abilitybar.transform);
            but.GetComponent<SkillButton>().SetImage(spell.IconImage);
            but.GetComponent<SkillButton>().SetSpellNameText(spell.GetNameText());
            but.GetComponent<SkillButton>().SetDamageText(spell.GetDamageText());
            but.GetComponent<SkillButton>().SetExtraText(spell.GetExtraText());
            but.GetComponent<SkillButton>().SetSkillNumber(i);
            Buttons.Add(but);
            i++;
        }
    }

    public override void DestroySkillHotbar() //destroy all buttons on skillbar
    {
        foreach (GameObject button in Buttons)
        {
            Destroy(button);
        }
        Buttons.Clear();
    }

    public void castSpell(int spellNumber, EnvironmentTile target) //cast spell at target tile
    {
        if (!HasAttacked && Spells[spellNumber].CanCast(target))
        {
            var audio = GetComponent<AudioSource>();
            audio.clip = SpellSound;
            audio.Play();
            StartCoroutine(DoCastSpell(spellNumber, target));
        }
        else
            base.finishAction();
    }

    public IEnumerator DoCastSpell(int spellNumber, EnvironmentTile target)
    {
        base.mAnimator.SetTrigger("Cast"); //coroutine so that its not an instant spell
        yield return Spells[spellNumber].cast(this, target);
        base.HasAttacked = true;
        base.finishAction();
    }
    // Update is called once per frame
    void Update()
    {
       
    }
}

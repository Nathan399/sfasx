﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuReset : MonoBehaviour
{
    public void ResetToMainMenu()
    {
        Game game = GameObject.Find("Game").GetComponent<Game>();
        game.ResetToMainMenu();
    }
}

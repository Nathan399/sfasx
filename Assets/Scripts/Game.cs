﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class Game : MonoBehaviour
{
    [SerializeField] private Camera MainCamera;
    [SerializeField] private List<PlayerCharacter> Characters; //list of character types
    [SerializeField] private Character Enemy; //enemy type
    [SerializeField] private Canvas Menu; //menu canvas
    [SerializeField] private Canvas Hud;
    [SerializeField] private Canvas GameOver; //gameover canvas
    [SerializeField] public GameUI GameUI; //ingame ui
    [SerializeField] private List<Transform> CharacterStart; //character start locations
    [SerializeField] private GameObject TileDecal; //hover tile decal
    [SerializeField] private GameObject SelectedDecal; //selected character decal

    private RaycastHit[] mRaycastHits;
    private List<PlayerCharacter> mCharacters; //list of created characters
    public PlayerCharacter mSelectedChar; //selected char
    private List<Character> mEnemys; //all of the enemies
    private Environment mMap; 

    private int BaseEnemyAmount = 3; //how many enemies to spawn
    private int TurnCounter = 0; //current player turn
    private int KillCounter = 0; //amount of zombies killed
    private int EnemyCharNumber = 0; //amount of enemies
    private readonly int NumberOfRaycastHits = 1;

    public int SelectedSkillNumber = 0;//selected char skill

    public UnityEvent mCharActionFinish;
    public UnityEvent mPlayerTurnEnd;
    public UnityEvent mEnemyTurnEnd;
    enum GameStates
    {
        MainMenu,
        PlayerTurn,
        PlayerAction,
        EnemyTurn,
        EnemyAction,
    }
    GameStates state = GameStates.MainMenu;

    void Start()
    {
        mRaycastHits = new RaycastHit[NumberOfRaycastHits];
        mMap = GetComponentInChildren<Environment>();
        //spawn characters
        mCharacters = new List<PlayerCharacter>(); 
        foreach(PlayerCharacter chara in Characters)
        {
            mCharacters.Add(Instantiate(chara, transform));
        }
        
        mEnemys = new List<Character>();
        mCharActionFinish = new UnityEvent();
        mCharActionFinish.AddListener(ActionFinished);
        mPlayerTurnEnd = new UnityEvent();
        mEnemyTurnEnd = new UnityEvent();
        GameOver.enabled = false;
        ShowMenu(true);
    }

    private void Update()
    {
        // Check to see if the player has clicked a tile and if they have, try to find a path to that 
        // tile. If we find a path then the character will move along it to the clicked tile. 
        Ray screenClick = MainCamera.ScreenPointToRay(Input.mousePosition);
        int hits;
        //layer mask to prioritise characters over the world
        int layerMask = LayerMask.GetMask("Characters");
        hits = Physics.RaycastNonAlloc(screenClick, mRaycastHits,1000, layerMask); 
        if (hits == 0)
            hits = Physics.RaycastNonAlloc(screenClick, mRaycastHits);

        switch (state)
        {
            case GameStates.PlayerTurn:
                {
                    if (hits > 0)
                    {
                        EnvironmentTile tile = mRaycastHits[0].transform.GetComponent<EnvironmentTile>();
                        Character charac = mRaycastHits[0].transform.GetComponent<PlayerCharacter>();
                        bool playerCharHover = false;

                        if (charac != null) // if a character is hovered with the mouse
                        {

                            playerCharHover = IsPlayerCharacter(charac); //check if the character is a player character

                            if (Input.GetMouseButtonDown(0) && playerCharHover)  //if left click a player character
                            {
                                if(mSelectedChar) //select it and show the information for it
                                    mSelectedChar.DestroySkillHotbar();
                                mSelectedChar = (PlayerCharacter)charac;
                                mMap.ResetMovementIndicators();
                                mSelectedChar.ShowSkillHotbar();
                                mSelectedChar.ShowMovement();
                            }

                            if (Input.GetMouseButtonDown(0) && charac != mSelectedChar && mSelectedChar && !playerCharHover && !EventSystem.current.IsPointerOverGameObject()) //if mouse is over a enemy
                            {
                                Debug.LogFormat("connections: {0}", charac.CurrentPosition.Connections.Count); //send selected char to attack enemy
                                List<EnvironmentTile> route = mMap.FindClosestNeighbour(mSelectedChar.CurrentPosition, charac.CurrentPosition.Connections);
                                state = GameStates.PlayerAction;
                                mSelectedChar.GoToAttack(route, charac);
                            }

                            TileDecal.SetActive(false);
                        }
                        else if (tile != null && tile.IsAccessible )
                        {
                            
                            if (Input.GetMouseButtonDown(0) && mSelectedChar && !EventSystem.current.IsPointerOverGameObject()) //if over a tile,
                            {
                                List<EnvironmentTile> route = mMap.Solve(mSelectedChar.CurrentPosition, tile); //send selected char to tile
                                state = GameStates.PlayerAction;
                                mSelectedChar.GoTo(route);
                            }
                            TileDecal.SetActive(true);
                            TileDecal.transform.position = tile.Position + new Vector3(0, 1, 0);
                        }

                        if (Input.GetMouseButtonDown(1)  && !EventSystem.current.IsPointerOverGameObject() && mSelectedChar) //cast spell at hower target
                        {
                            bool charTile = false;
                            if (charac) //if hovering character get tile
                            {
                                tile = charac.CurrentPosition;
                                charTile = true;
                            }

                            if (tile && (tile.IsAccessible || charTile))
                            {
                                state = GameStates.PlayerAction;
                                mSelectedChar.castSpell(SelectedSkillNumber, tile); //cast spell at tile
                            }
                        }

                    }
                    else
                        TileDecal.SetActive(false);
                    break;
                }
            case GameStates.PlayerAction:
                {
                    TileDecal.SetActive(false);
                    break;
                }
            case GameStates.EnemyTurn:
                {
                    CleanEnemyList(); //check everyone is alive in the lists
                    CheckPlayersAlive();
                    if (mEnemys.Count != 0 && EnemyCharNumber < mEnemys.Count) 
                    {
                        List<EnvironmentTile> ClosestCharRoute = new List<EnvironmentTile>();
                        bool first = true;
                        PlayerCharacter target = null;
                        int i = 0;
                        foreach (PlayerCharacter chara in mCharacters) //loop through all characters to find the closest one 
                        {
                            if(chara)
                            {
                                List<EnvironmentTile> route = mMap.FindClosestNeighbour(mEnemys[EnemyCharNumber].CurrentPosition, chara.CurrentPosition.Connections);
                                if (route != null)
                                {
                                    if (first || ClosestCharRoute.Count > route.Count)
                                    {
                                        ClosestCharRoute = route;
                                        target = chara;
                                        first = false;
                                    }
                                }
                            }
                            i++;
                        }

                        state = GameStates.EnemyAction;
                        if (ClosestCharRoute != null && target) //if there is a route to an enemy attack it otherwise do nothing
                            mEnemys[EnemyCharNumber].GoToAttack(ClosestCharRoute, target);
                        else
                            mEnemys[EnemyCharNumber].Idle();
                        
                    }
                    else
                        ResetToPlayerTurn();

                    break;
                }
            case GameStates.EnemyAction:
                {
                    break;
                }

            
        }

        if (state == GameStates.PlayerTurn && mSelectedChar) //set selected char decal on selected player
        {
            SelectedDecal.SetActive(true);
            SelectedDecal.transform.position = mSelectedChar.transform.position + new Vector3(0,1,0);
        }
        else
            SelectedDecal.SetActive(false);

        CheckPlayersAlive();
    }   

    public void ShowMenu(bool show)
    {
        if (Menu != null && Hud != null)
        {
            Menu.enabled = show;
            Hud.enabled = !show;
            GameUI.gameObject.SetActive(!show);

            if (show)
            {
                for(int Counter = 0; Counter < CharacterStart.Count; Counter++) //reset all character positions
                {
                    mCharacters[Counter].transform.position = CharacterStart[Counter].position;
                    mCharacters[Counter].transform.rotation = CharacterStart[Counter].rotation;
                    mCharacters[Counter].CurrentMove = 0;
                }
                
                mMap.CleanUpWorld();
            }
            else
            {
                foreach (PlayerCharacter chara in mCharacters) //set all character positions in the world
                {
                    EnvironmentTile start = mMap.GetRandomTile();
                    chara.transform.position = start.Position;
                    chara.transform.rotation = Quaternion.identity;
                    chara.ResetTurnValues();
                    chara.CurrentPosition = start;
                }
                    
            }
        }
    }

    public void Generate()
    {
        mMap.GenerateWorld();
        state = GameStates.PlayerTurn;
        GenerateEnemies(BaseEnemyAmount);
    }

    public void GenerateEnemies(int amount)
    {
        for(int counter = 0; counter < amount; counter++) //create enemy at a random tile
        {
            EnvironmentTile tile = mMap.GetRandomTile();
            Quaternion q = Quaternion.LookRotation(Vector3.back, Vector3.up);
            tile.IsAccessible = false;

            mEnemys.Add(Instantiate(Enemy, tile.Position, q));
            mEnemys[mEnemys.Count - 1].CurrentPosition = tile;
        }
    }

    public void Exit()
    {
#if !UNITY_EDITOR
        Application.Quit();
#endif
    }

    public void ResetToMainMenu()
    {
        if (mSelectedChar) //clear hotbar if there is a selected char
            mSelectedChar.DestroySkillHotbar();

        foreach (Character enemy in mEnemys)
        {
            Destroy(enemy.gameObject);
        }
        mEnemys.Clear();

        GameOver.enabled = false;

        //reset characters
        foreach (PlayerCharacter chara in mCharacters)
        {
            Destroy(chara.gameObject);
        }
        mCharacters.Clear();

        //create new characters
        foreach (PlayerCharacter chara in Characters)
        {
            mCharacters.Add(Instantiate(chara, transform));
        }
        //reset values
        state = GameStates.MainMenu;
        TurnCounter = 0;
        KillCounter = 0;
        ShowMenu(true);
    }

    private void ActionFinished()
    {
        if (state == GameStates.PlayerAction)
        {
            bool AllFinished = true;
            foreach (PlayerCharacter chara in mCharacters) //check if all characters have finished their actions
            {
                if (!chara.IsFinished()) 
                    AllFinished = false;
            }

            if (AllFinished) //if they have set the turn to enemy turn and reset values
            {
                GameUI.ChangeToEnemyTurn();
                state = GameStates.EnemyTurn;
                for (int counter = 0; counter < mEnemys.Count; counter++)
                    mEnemys[counter].ResetTurnValues();
                mMap.ResetMovementIndicators();
                mPlayerTurnEnd.Invoke();
                TurnCounter++;
            }
            else
            {
                state = GameStates.PlayerTurn; //otherwise finish player turn
                mMap.ResetMovementIndicators();
                mSelectedChar.ShowMovement();
            }
            
        }

        if (state == GameStates.EnemyAction) 
        {
            CheckPlayersAlive();
            EnemyCharNumber++;
            if (EnemyCharNumber >= mEnemys.Count) //if every enemy has acted, reset to player turn
            {
                ResetToPlayerTurn();
            }
            else
                state = GameStates.EnemyTurn;
        }
    }

    private void CheckPlayersAlive()
    {
        for (int i = mCharacters.Count - 1; i >= 0; i--) //if characters are dead removes them form the lsit
        {
            if (mCharacters[i] == null)
            {
                mCharacters.RemoveAt(i);
            }
        }
        
        if (mCharacters.Count == 0) //if there are no player characters left end game
        {
            GameOver.enabled = true;
            GameOver.GetComponent<GameOverScript>().SetText(TurnCounter, KillCounter);
        }
    }

    private void ResetToPlayerTurn()
    {
        state = GameStates.PlayerTurn; //reset all character turn values
        GameUI.ChangeToPlayerTurn();
        foreach (PlayerCharacter chara in mCharacters)
        {
            chara.ResetTurnValues();
        }
        if(mSelectedChar)
            mSelectedChar.ShowMovement();
        mEnemyTurnEnd.Invoke();
        EnemyCharNumber = 0;
        SpawnEnemies();
    }

    private void SpawnEnemies()
    {
        if(TurnCounter % 3 == 2) //spawn enemies every 3 rounds
        {
            GenerateEnemies(BaseEnemyAmount);
        }
    }

    private void CleanEnemyList()
    {
        for(int i = mEnemys.Count -1; i >=0; i--) //remove enemies from list if they are dead
        {
            if (mEnemys[i] == null)
            {
                mEnemys.RemoveAt(i);
                KillCounter++;
            }
        }
    }

    public bool IsPlayerCharacter(Character chara) //check if a character is in the player character list
    {
        foreach (PlayerCharacter charac in mCharacters)
        {
            if (charac == chara)
                return true;
        }
        return false;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStatBar : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] List<GameObject> MoveIcons;
    PlayerCharacter character;

    void Start()
    {
        character = transform.parent.GetComponent<PlayerCharacter>();
    }

    // Update is called once per frame
    void Update()
    {
        Camera camera = Camera.main; //have the movement indicators look at camera
        transform.LookAt(transform.position + camera.transform.rotation * Vector3.forward, camera.transform.rotation * Vector3.up);

        for(int counter = 0; counter < MoveIcons.Count; counter++) //remove movement indicators when they have less than that amount
        {
            if (character.CurrentMove <= counter)
                MoveIcons[counter].SetActive(false);
            else
                MoveIcons[counter].SetActive(true);
        }
    }
}

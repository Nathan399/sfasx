﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.Animations;

[System.Serializable]
public class Character : MonoBehaviour
{
    [SerializeField] private float SingleNodeMoveTime = 0.5f; //time it takes to move to the next tile
    [SerializeField] private float Maxhealth; //characters max health
    [SerializeField] private int MaxMove; //amount of tiles character can move a turn
    [SerializeField] private int AttackDamage; //amount of damage the character does in melee
    [SerializeField] private AudioClip AttackSound; //sound of melee attack
    [SerializeField] private AudioClip TakeDamageSound; //sound when character takes damage
    [SerializeField] protected bool HasSpawnEffect; //if the character has an effect when they are created
    [SerializeField] protected bool HasDeathEffect; //if the character has an effect when they are destroyed

    public int CurrentMove; //how many more time they can move
    protected bool HasAttacked; //if they have attacked

    private float _Health; //current hp

    public float Health
    {
        get { return _Health; }
        set
        {
            _Health = value;
            if (_Health >= Maxhealth) //hide health bar if they are max health 
                HealthUI.gameObject.SetActive(false);
            else
            {
                HealthUI.gameObject.SetActive(true);
                HealthUI.setPercentage(Health / Maxhealth);
            }
        }
    }

    protected Animator mAnimator;

    private UIHealthBar HealthUI; //health bar

    private EnvironmentTile _CurrentPosition;
    public EnvironmentTile CurrentPosition
    {
        get { return _CurrentPosition; }
        set
        {
            if (_CurrentPosition != null) //make the current tile they are on inaccessible so people cant walk through them
            {
                _CurrentPosition.IsAccessible = true;
                _CurrentPosition.CharOn = null;
            }
            _CurrentPosition = value;
            if(_CurrentPosition != null)
            {
                _CurrentPosition.IsAccessible = false;
                _CurrentPosition.CharOn = this;
            }
        }
    }

    public virtual void Awake()
    {
        HealthUI = transform.Find("HealthCanvas").GetComponent<UIHealthBar>(); //get characters health bar
        Health = Maxhealth;
        CurrentMove = MaxMove;
        HasAttacked = false;
        mAnimator = GetComponent<Animator>();

        if(HasSpawnEffect)
        {
            var deathEffect = Resources.Load("DeathEffect") as GameObject; //load in the pawn and death effect
            var effect = Instantiate(deathEffect, transform);
            effect.transform.localScale = new Vector3(1 / transform.localScale.x, 1 / transform.localScale.y, 1 / transform.localScale.z);

            Destroy(effect, 5.0f);
        }
    }

    private IEnumerator DoMove(Vector3 position, Vector3 destination)
    {
        // Move between the two specified positions over the specified amount of time
        if (position != destination)
        {
            transform.rotation = Quaternion.LookRotation(destination - position, Vector3.up);

            Vector3 p = transform.position;
            float t = 0.0f;

            while (t < SingleNodeMoveTime)
            {
                t += Time.deltaTime;
                p = Vector3.Lerp(position, destination, t / SingleNodeMoveTime);
                transform.position = p;
                yield return null;
            }
            CurrentMove--;
        }
    }

    private IEnumerator DoGoTo(List<EnvironmentTile> route, bool finalAction)
    {
        mAnimator.SetBool("Walking", true);
        // Move through each tile in the given route
        if (route != null)
        {
            Vector3 position = CurrentPosition.Position;
            for (int count = 0; count < route.Count; ++count)
            {
                if (CurrentMove > 0)
                {
                    Vector3 next = route[count].Position;
                    yield return DoMove(position, next);
                    CurrentPosition = route[count];
                    position = next;
                    Debug.LogFormat("moveAmount: {0}", CurrentMove);
                }
            }
        }
        mAnimator.SetBool("Walking", false);
        if (finalAction)
            finishAction();
    }

    private IEnumerator DoGoToAttack(List<EnvironmentTile> route, Character target)
    {
        //move to a target and if you end next to them attack them
        if(!HasAttacked)
        {
            yield return DoGoTo(route, false);
            if (CurrentPosition == route[route.Count - 1])
            {
                transform.LookAt(target.transform.position);
                mAnimator.SetTrigger("Attack");
                var audio = GetComponent<AudioSource>();
                audio.clip = AttackSound;
                audio.Play();
                target.TakeDamage(AttackDamage);
                HasAttacked = true;
            }
        }
        finishAction();
    }

    public void GoTo(List<EnvironmentTile> route)
    {
        // Clear all coroutines before starting the new route so 
        // that clicks can interupt any current route animation
        StopAllCoroutines();
        StartCoroutine(DoGoTo(route, true));
    }


    public void GoToAttack(List<EnvironmentTile> route, Character target)
    {
        //clear all corutines and then move to target and attack
        StopAllCoroutines();
        StartCoroutine(DoGoToAttack(route, target));
    }

    public void Idle()
    {
        finishAction();//do nothing for the turn
    }
    public void TakeDamage(float damage)
    {
        Health -= damage; //lower health
        var audio = GetComponent<AudioSource>(); //play the take damage noise
        audio.clip = TakeDamageSound;
        audio.Play();
        if (Health <= 0)
        {
            CurrentPosition = null; //if they have died set it so the tile can be walked on
            Destroy(gameObject);
            DestroySkillHotbar();
        }
        

        var FadeText = Resources.Load("FadeText") as GameObject; //play text animation of how much damage they took
        var text = Instantiate(FadeText, transform.position, Quaternion.identity);
        text.GetComponent<TMPro.TextMeshPro>().text =  damage.ToString();
        text.GetComponent<TMPro.TextMeshPro>().faceColor = Color.red;
        Destroy(text, text.GetComponent<Animation>().clip.length);
    }

    public void Heal(float Amount)
    {
        Health += Amount;

        if (Health > Maxhealth) //health cannot go past max
            Health = Maxhealth;

        var FadeText = Resources.Load("FadeText") as GameObject; //play text animation of how much damage they healed
        var text = Instantiate(FadeText, transform.position , Quaternion.identity);
        text.GetComponent<TMPro.TextMeshPro>().text = Amount.ToString();
        text.GetComponent<TMPro.TextMeshPro>().faceColor = Color.green;
        Destroy(text, text.GetComponent<Animation>().clip.length);
    }

    public void ShowMovement()
    {
        for (int count = 0; count < CurrentPosition.Connections.Count; ++count) //make the spheres appear where the character can move
        {
           CurrentPosition.Connections[count].ShowMoveable(CurrentMove);
        }
    }

    protected void finishAction()
    {
        if (HasAttacked) //if the character has attack or move make it so they cant act
            CurrentMove = 0;
        if (CurrentMove == 0)
            HasAttacked = true;

        Game game = GameObject.Find("Game").GetComponent<Game>(); //tell the game the character has finished their action
        game.mCharActionFinish.Invoke();
    }

    public void ResetTurnValues()
    {
        CurrentMove = MaxMove; //reset chracter turn values to their base
        HasAttacked = false;
    }

    public bool IsFinished()
    {
        if (CurrentMove == 0 || HasAttacked == true) //if the player has no movement or has attack their turn has ended
            return true;
        return false;
    }

    public virtual void ResetCharacter() 
    {
        Health = Maxhealth; //reset all of the characters values to their base
        ResetTurnValues();
    }

    public virtual void ShowSkillHotbar() {}
    public virtual void DestroySkillHotbar() {}

    private void OnDestroy()
    {
        if (HasDeathEffect && Health <= 0) //when the character dies, make a death effect
        {
            var deathEffect = Resources.Load("DeathEffect") as GameObject;
            var effect = Instantiate(deathEffect, transform.position + new Vector3(0,2.5f,0),transform.rotation);

            Destroy(effect, 5.0f);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScript : MonoBehaviour
{
    // Start is called before the first frame update
    TMPro.TextMeshProUGUI roundText;
    TMPro.TextMeshProUGUI killText;
    

    void Start()
    {
        roundText = transform.Find("RoundText").GetComponent<TMPro.TextMeshProUGUI>();
        killText = transform.Find("KillText").GetComponent<TMPro.TextMeshProUGUI>();
    }
    public void SetText(int round, int kill)
    {
        roundText.text = "You have survived " + round + " rounds"; //set the endgame screen values
        killText.text = "You have killed " + kill + " zombies";
    }
}

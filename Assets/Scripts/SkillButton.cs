﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillButton : MonoBehaviour
{
    // Start is called before the first frame update
    GameObject ToolTip;
    Text[] texts;
    int SkillNumber = 0;
    void Awake()
    {
        ToolTip = transform.Find("ToolTip").gameObject;
        texts = ToolTip.GetComponentsInChildren<Text>();
    }

    //set texts for tool tip
    public void SetSpellNameText(string text)
    {
        texts[0].text = text;
    }
    public void SetDamageText(string text)
    {
        texts[1].text = text;
    }
    public void SetExtraText(string text)
    {
        texts[2].text = text;
    }

    //set button image
    public void SetImage(Sprite image)
    {
        GetComponent<Image>().sprite = image;
    }
    public void SetSkillNumber(int skillnumber) //skill number for what it is in characters list
    {
        SkillNumber = skillnumber;
    }

    public void SelectSkill()
    {
        Game game = GameObject.Find("Game").GetComponent<Game>();
        game.SelectedSkillNumber = SkillNumber;
    }
       
    public void ShowToolTip()
    {
        //If your mouse hovers over the GameObject with the script attached, output this message
        Debug.Log("Mouse is over GameObject.");
        ToolTip.SetActive(true);
    }

    public void HideToolTip()
    {
        //The mouse is no longer hovering over the GameObject so output this message each frame
        Debug.Log("Mouse is no longer on GameObject.");
        ToolTip.SetActive(false);
    }
}

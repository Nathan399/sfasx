﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentTile : MonoBehaviour
{
    public List<EnvironmentTile> Connections { get; set; }
    public EnvironmentTile Parent { get; set; }
    public Vector3 Position { get; set; }
    public float Global { get; set; }
    public float Local { get; set; }
    public bool Visited { get; set; }
    public bool IsAccessible { get; set; }

    public GameObject DecalMoveable;

    public Character CharOn = null; //character stood on tile 

    public int dist = 0;
    private void Awake()
    {
        DecalMoveable = GameObject.CreatePrimitive(PrimitiveType.Sphere); //create the sphere that shows if they can move
        DecalMoveable.transform.position = transform.position + new Vector3(5,2.5f,5);
        DecalMoveable.transform.localScale = 1.3f * Vector3.one;
        DecalMoveable.SetActive(false);
    }
    public void ShowMoveable(int distance)
    {
        if (distance > 0 && IsAccessible)
        {
            dist = distance; //check if the char can move on this.
            DecalMoveable.SetActive(true);
            if (distance == 1) //if it would be its final move make the sphere red
                DecalMoveable.GetComponent<MeshRenderer>().material = Resources.Load<Material>("FinalMoveMaterial");
            else //else make it white
                DecalMoveable.GetComponent<MeshRenderer>().material = Resources.Load<Material>("MoveableMaterial"); 

            distance--; //recusivly call neighbours tiles
            for (int count = 0; count < Connections.Count; ++count)
            {
                if(Connections[count].dist < distance)
                    Connections[count].ShowMoveable(distance);
            }
        }
    }

    void OnDestroy()
    {
       Destroy(DecalMoveable);
    }
}

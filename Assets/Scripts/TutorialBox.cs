﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialBox : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] List<string> TutorialTips;
    [SerializeField] Text TutorialTextBox;
    [SerializeField] GameObject NextButton;
    [SerializeField] GameObject PreviousButton;

    int tipNumber = 0;
    bool shown = true;

    private void Awake()
    {
        PreviousButton.SetActive(false);
        TutorialTextBox.text = TutorialTips[0];
    }
    public void NextTip()
    {
        tipNumber++;
        TutorialTextBox.text = TutorialTips[tipNumber]; //get the next tip to put into the box
        if (tipNumber >= TutorialTips.Count - 1)        //if it is last tip hide button
            NextButton.SetActive(false);
        PreviousButton.SetActive(true);
    }

    public void PreviousTip()
    {
        tipNumber--;
        TutorialTextBox.text = TutorialTips[tipNumber]; //get the next tip to put into the box
        if (tipNumber == 0)                             //if it is first tip hide button
            PreviousButton.SetActive(false);
        NextButton.SetActive(true);
    }

    public void ShowHide()
    {
        if(shown)
            GetComponent<Animator>().SetTrigger("Hide"); //hide or show tutorial box
        else
            GetComponent<Animator>().SetTrigger("Show");

        shown = !shown;
    }
}

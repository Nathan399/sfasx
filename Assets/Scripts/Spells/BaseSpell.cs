﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
//spell sandbox class
public abstract class BaseSpell : MonoBehaviour
{
    [SerializeField]
    protected GameObject Projectile; //spell effect game object
    [SerializeField]
    public Sprite IconImage; //icon for the skillbar

    protected string NameText = "spell"; //tool tip texts
    protected string DamageText = "damage";
    protected string ExtraText = "extra";

    protected float SpellSpeed = 1.0f; //speed at which the spell travels
    protected float SpellDamage = 20.0f; //damage of spell
    protected bool NeedsCharTarget = true; //can the spell target the envrionment
    protected bool CanSelfCast = false; //can the spell be cast on the caster

    //base parts of a spell
    public abstract IEnumerator cast(PlayerCharacter caster, EnvironmentTile target);

    //helper functions
    protected IEnumerator FireProjectile(PlayerCharacter caster, PlayerCharacter target)
    {
        Vector3 Startpos = caster.transform.position + new Vector3(0.0f, 5.0f, 0.0f); //fires a projectile to a target location
        Vector3 endpos = target.transform.position + new Vector3(0.0f, 5.0f, 0.0f);

        GameObject proj = Instantiate(Projectile, Startpos, caster.transform.rotation);

        proj.transform.rotation = Quaternion.LookRotation(proj.transform.position - Startpos, Vector3.up);
        float t = 0.0f;

        while (t < SpellSpeed)
        {
            t += Time.deltaTime;
            proj.transform.position = Vector3.Lerp(Startpos, endpos, t / SpellSpeed);
            yield return null;
        }

        stopEmittingParticles(proj); //stop the particle emitting before it is destroyed so it doesnt instantly dissapear
        Destroy(proj, 5.0f);
    }

    protected IEnumerator FireProjectile(Vector3 Startpos, Vector3 EndPos)
    {
        GameObject proj = Instantiate(Projectile, Startpos, transform.rotation); // uses positions instead of a target and tile

        proj.transform.rotation = Quaternion.LookRotation(proj.transform.position - Startpos, Vector3.up);
        float t = 0.0f;

        while (t < SpellSpeed)
        {
            t += Time.deltaTime;
            proj.transform.position = Vector3.Lerp(Startpos, EndPos, t / SpellSpeed);
            yield return null;
        }

        stopEmittingParticles(proj); //stop the particle emitting before it is destroyed so it doesnt instantly dissapear
        Destroy(proj, 5.0f);
    }

    protected void stopEmittingParticles(GameObject proj)
    {
        ParticleSystem[] particleSystems = proj.GetComponentsInChildren<ParticleSystem>();  //stop the particle systems from emitting

        foreach(ParticleSystem systems in particleSystems)
        {
            systems.Stop(true, ParticleSystemStopBehavior.StopEmitting);
        }
    }

    //check if the player can cast the spell on a tile
    public virtual bool CanCast(EnvironmentTile tile)
    {
        bool isCastable = true;
        if(NeedsCharTarget) //if the spell needs to target a player
        {
            if (CanSelfCast) //check if the spell can be cast on the caster
            {
                if (TileHasChar(tile))
                    isCastable = true;
                else
                    isCastable = false;
            }
            else
            {
                if (TileHasChar(tile) && !TileIsSelectedChar(tile))
                    isCastable = true;
                else
                    isCastable = false;

                
            }
        }
        return isCastable;
    }

    protected bool TileHasChar(EnvironmentTile tile)
    {
        if (tile.CharOn) //check if tile has a character on it
            return true;
        return false;
    }

    protected bool TileIsSelectedChar(EnvironmentTile tile)
    {
        if (tile.CharOn == GameObject.Find("Game").GetComponent<Game>().mSelectedChar) // check if the tiles character is the caster
            return true;
        return false;
    }

    protected void SetTargetOnFire(PlayerCharacter target)
    {
        Damp comp = target.gameObject.GetComponent<Damp>();
        if (comp != null) //if target is damp dont add fire and remove damp
        {
            Destroy(comp);
            var SteamEffect = Resources.Load("SteamEffect") as GameObject; //add a steam effect to show the target is no longer damp
            var Effect = Instantiate(SteamEffect, target.transform);
            Destroy(Effect, 5.0f);
        }
        else
        {
            if (!target.gameObject.GetComponent<OnFire>())//check target isnt already on fire
                target.gameObject.AddComponent<OnFire>();
        }
    }

    protected void WetTarget(PlayerCharacter target)
    {
        OnFire comp = target.gameObject.GetComponent<OnFire>();
        if (comp != null)
        {
            Destroy(comp);
            var SteamEffect = Resources.Load("SteamEffect") as GameObject; //add a steam effect to show the target is no longer on fire
            var Effect = Instantiate(SteamEffect, target.transform);
            Destroy(Effect, 5.0f);
        }
        else
        {
            if (!target.gameObject.GetComponent<Damp>())//check target isnt already damp
                target.gameObject.AddComponent<Damp>();
        }
    }

    public string GetDamageText()
    {
        return DamageText; //returns the damage text of the spell
    }

    public string GetNameText()
    {
        return NameText; //returns the name of the spell
    }

    public string GetExtraText()
    {
        return ExtraText; //returns the extra details of the spell
    }

}


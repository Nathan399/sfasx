﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damp : BaseDebuff
{
    // Start is called before the first frame update
    void Start()
    {
        var DampEffect = Resources.Load("Damp") as GameObject; //load and spawn the flame effect
        Attached = gameObject.GetComponent<Character>();
        DebuffEffect = Instantiate(DampEffect, Attached.transform);
        SetScale();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnDestroy()
    {
        Destroy(DebuffEffect); //destroy the damp particle system
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningCloud : BaseDebuff
{
    // Start is called before the first frame update
    int CloudDamage = 25;
    void Start()
    {
        SetOnPlayerTurnEnd(Effect);
        var lightningCloudEffect = Resources.Load("LightningCloud") as GameObject; //create cloud particle system
        Attached = gameObject.GetComponent<Character>();
        DebuffEffect = Instantiate(lightningCloudEffect, Attached.transform);
        SetScale();
    }

    private void Effect()
    {
        if(Attached.GetComponent<Damp>()) //if target is wet, deal double damage
            Attached.TakeDamage(CloudDamage * 2);
        else
            Attached.TakeDamage(CloudDamage);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

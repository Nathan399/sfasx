﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnFire : BaseDebuff
{
    // Update is called once per frame
    int FireDamage = 30;
    void Awake()
    {
        SetOnPlayerTurnEnd(Effect);
        var vflame = Resources.Load("Flame") as GameObject; //create flame effect
        Attached = gameObject.GetComponent<Character>();
        DebuffEffect = Instantiate(vflame, Attached.transform);
        SetScale();
    }
    private void Effect()
    {
        Attached.TakeDamage(FireDamage);//deal damage to attacked target
    }

    private void OnDestroy()
    {
        Destroy(DebuffEffect); //destroy effect
    }
}

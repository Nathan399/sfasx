﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//sandbox class
public class BaseDebuff : MonoBehaviour
{
    protected GameObject DebuffEffect;
    protected Character Attached;
    protected void SetOnPlayerTurnEnd(UnityEngine.Events.UnityAction function)
    {
        Game game = GameObject.Find("Game").GetComponent<Game>(); //set passed function to be called on the end of the players turn
        game.mPlayerTurnEnd.AddListener(function);
    }

    protected void SetScale()
    {
        DebuffEffect.transform.localScale = new Vector3(1/transform.localScale.x, 1 / transform.localScale.y, 1 / transform.localScale.z);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FireBall : BaseSpell
{
    // Start is called before the first frame update
    void Start()
    {
        SpellDamage = 10; //set up spell values
        NameText = "FireBall";
        DamageText = "Damage: " + SpellDamage;
        ExtraText = "Sets target on fire";
    }

    public override IEnumerator cast(PlayerCharacter caster,EnvironmentTile target) 
    {
        yield return spell(caster, (PlayerCharacter)target.CharOn); //cast the spell
    }

    private IEnumerator spell(PlayerCharacter caster, PlayerCharacter target)
    {
        yield return FireProjectile(caster, target); //fire the projectile
        target.TakeDamage(SpellDamage); //after it hits the target make them take damage and set them on fire
        SetTargetOnFire(target);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal : BaseSpell
{
    // Start is called before the first frame update
    void Start()
    {
        NameText = "Heal"; //set up spell values
        DamageText = "Heals: " + SpellDamage;
        ExtraText = "Cannot go over max health";
        CanSelfCast = true;
    }

    public override IEnumerator cast(PlayerCharacter caster, EnvironmentTile target)
    {
        yield return spell(caster, (PlayerCharacter)target.CharOn); //cast spell
    }

    private IEnumerator spell(PlayerCharacter caster, PlayerCharacter target)
    {
        target.Heal(SpellDamage); //instantly heal the target

        var healEffect = Instantiate(Projectile, target.transform); //play the heal spell effect
        Destroy(healEffect, 3.0f);

        yield return null;
    }
    public override bool CanCast(EnvironmentTile tile)
    {
        if (tile.CharOn) //spell can only be cast on friendly characters
        {
            Game game = GameObject.Find("Game").GetComponent<Game>();
            return game.IsPlayerCharacter(tile.CharOn);
        }
        return false;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningBall : BaseSpell

{
    // Start is called before the first frame update
    void Start()
    {
        SpellDamage = 25; //set up spell values
        NameText = "Lightning Ball";
        DamageText = "Damage: " + SpellDamage;
        ExtraText = "Deals double damage to wet enemies";
    }

    public override IEnumerator cast(PlayerCharacter caster, EnvironmentTile target)
    {
        yield return spell(caster, (PlayerCharacter)target.CharOn);
    }

    private IEnumerator spell(PlayerCharacter caster, PlayerCharacter target)
    {
        yield return FireProjectile(caster, target); 
        
        Damp comp = target.gameObject.GetComponent<Damp>();
        if (comp != null) //if target is damp deal double damage
            target.TakeDamage(SpellDamage * 2);
        else
            target.TakeDamage(SpellDamage);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : BaseSpell
{
    // Start is called before the first frame update
    void Start()
    {
        SpellDamage = 5;
        NameText = "Meteor";
        DamageText = "Damage: " + SpellDamage;
        ExtraText = "Sets target on fire in an area";
    }

    public override IEnumerator cast(PlayerCharacter caster, EnvironmentTile target)
    {
        yield return spell(caster, target);
    }

    private IEnumerator spell(PlayerCharacter caster, EnvironmentTile target)
    {
        //fire projectile from above the target
        yield return FireProjectile(target.transform.position + new Vector3(-5, 100, -5), target.transform.position + new Vector3(5, 0, 5)); 
        var vExplosion = Resources.Load("Explosion") as GameObject;

        if (target.CharOn) //if there is a target on selected tile deal damage
        {
            target.CharOn.TakeDamage(SpellDamage);
            SetTargetOnFire((PlayerCharacter)target.CharOn);
        }

        var neighbours = target.Connections;

        foreach(EnvironmentTile tile in neighbours) //if there is a target on the neighbours deal damage to them too
        {
            if(tile.CharOn)
            {
                tile.CharOn.TakeDamage(SpellDamage);
                SetTargetOnFire((PlayerCharacter)tile.CharOn);
            }
            var explosion = Instantiate(vExplosion, tile.transform); //play explosion on neighbour tiles
            explosion.transform.position += new Vector3(5, 0, 5);
            Destroy(explosion, 5.0f);
        }
    }

    public override bool CanCast(EnvironmentTile tile)
    {
        return true; //always can cast
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningCloudSpell : BaseSpell
{
    // Start is called before the first frame update
    void Start()
    {
        NameText = "Lightning Cloud";
        DamageText = "Applies Cloud";
        ExtraText = "Cloud deals double damage to wet enemies";
    }

    public override IEnumerator cast(PlayerCharacter caster, EnvironmentTile target)
    {
        yield return spell(caster, (PlayerCharacter)target.CharOn);
    }

    private IEnumerator spell(PlayerCharacter caster, PlayerCharacter target)
    {
        LightningCloud comp = target.gameObject.GetComponent<LightningCloud>();
        if (comp == null) //if target is damp deal double damage
            target.gameObject.AddComponent<LightningCloud>();

        yield return null;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WaterBall : BaseSpell
{
    void Start()
    {
        SpellDamage = 15; //set up spell values
        NameText = "Water Ball";
        DamageText = "Damage: " + SpellDamage;
        ExtraText = "Wets the target";
    }

    public override IEnumerator cast(PlayerCharacter caster, EnvironmentTile target)
    {
        yield return spell(caster, (PlayerCharacter)target.CharOn);
    }

    private IEnumerator spell(PlayerCharacter caster, PlayerCharacter target)
    {
        yield return FireProjectile(caster, target); //fire projectile and wet the target
        target.TakeDamage(SpellDamage);
        WetTarget(target);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class UIHealthBar : MonoBehaviour
{
    public Transform HealthBar;
    public Image ui;
    private void Awake()
    {
        HealthBar = transform.Find("HealthBar");
    }
    void Update()
    {
        Camera camera = Camera.main; //health bar looks at camera
        transform.LookAt(transform.position + camera.transform.rotation * Vector3.forward, camera.transform.rotation * Vector3.up);
    }

    public void setPercentage(float percentage)
    {
        HealthBar.transform.localScale = new Vector3(percentage, HealthBar.transform.localScale.y, HealthBar.transform.localScale.z); //set fill percentage for hotbar
    }
}
